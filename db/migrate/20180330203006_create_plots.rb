class CreatePlots < ActiveRecord::Migration[5.1]
  def change
    create_table :plots do |t|
      t.string :plot_name
      t.string :farm_locate
      t.string :area_plot
      t.text :notes
      t.integer :status

      t.timestamps
    end
  end
end

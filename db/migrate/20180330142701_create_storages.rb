class CreateStorages < ActiveRecord::Migration[5.1]
  def change
    create_table :storages do |t|
      t.string :storage_name
      t.string :farm_pertaining
      t.string :farm_meets
      t.text :notes
      t.integer :status

      t.timestamps
    end
  end
end

class CreateFarms < ActiveRecord::Migration[5.1]
  def change
    create_table :farms do |t|
      t.string :name_farm
      t.integer :area_total
      t.integer :area_productive
      t.integer :reserve_recorded
      t.integer :status
      t.text :notes

      t.timestamps
    end
  end
end

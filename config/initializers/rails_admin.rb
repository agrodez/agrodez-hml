RailsAdmin.config do |config|

  ### Popular gems integration

  ## == Devise ==
  # config.authenticate_with do
  #   warden.authenticate! scope: :user
  # end
  # config.current_user_method(&:current_user)

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  ## == Gravatar integration ==
  ## To disable Gravatar integration in Navigation Bar set to false
  # config.show_gravatar = true

  config.model Farm do
    list do
      field :name_farm
      field :area_total
      field :area_productive
      field :reserve_recorded
      exclude_fields :id, :status

    end
  end

  config.model Plot do
    list do
      field :plot_name
      field :farm_locate
      field :plot_area
      field :notes
      exclude_fields :id, :status

    end
  end

  config.model Storage do
    list do
      field :storage_name
      field :farm_pertaining
      field :farm_meets
      field :notes
      exclude_fields :id, :status

    end
  end

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end
end

class Storage < ApplicationRecord
  enum status: [:active, :inactive]
  has_many :farms
end

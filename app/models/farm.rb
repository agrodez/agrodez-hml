class Farm < ApplicationRecord
  enum status: [:active, :inactive]
  has_many :plots
  has_many :storages
end
